/**
 * Module Description: This module will contain functions to keep track of the number of
 *                     swaps/comparisons being utilized by a sorting algorithm.
 * 
 * Revision History
 * Date         Ver   Author            Description
 * 6/21/2021    1.0   richard.lam       Initial Version
 */

var SWAP_AND_COMPARISON_MODULE = (function() {

    let SWAP_AND_COMPARISON_FUNCTIONS = {};

    let totalSwaps = 0;
    let totalComparisons = 0;
    
    /**
    * @desc Add to total number of swaps done by current algorithm
    * @param {Number} numOfSwaps - num of swaps to add
    */
    SWAP_AND_COMPARISON_FUNCTIONS.addSwaps = function(numOfSwaps) {
        totalSwaps += numOfSwaps;
        SWAP_AND_COMPARISON_MODULE.updateStatisticsDisplay();
    }
    
    /**
    * @desc Add to total number of comparisons done by current algorithm
    * @param {Number} numOfComparisons - num of comparisons to add
    */
    SWAP_AND_COMPARISON_FUNCTIONS.addComparisons = function(numOfComparisons) {
        totalComparisons += numOfComparisons;
        SWAP_AND_COMPARISON_MODULE.updateStatisticsDisplay();
    }
    
    /**
    * @desc Reset total number of swaps and comparisons to 0
    * @param none
    */
    SWAP_AND_COMPARISON_FUNCTIONS.resetSwapsAndComparisons = function() {
        totalSwaps = 0;
        totalComparisons = 0;
    }
    
    /**
    * @desc Update comparisons and swaps shown on UI to value of totalSwaps and totalComparisons
    * @param {Number} None
    */
    SWAP_AND_COMPARISON_FUNCTIONS.updateStatisticsDisplay = function() {
        let swapsContainer = document.getElementById("num-of-swaps");
        let comparisonsContainer = document.getElementById("num-of-comparisons");
        
        swapsContainer.innerHTML = totalSwaps;
        comparisonsContainer.innerHTML = totalComparisons;
    }

    return SWAP_AND_COMPARISON_FUNCTIONS;
})();