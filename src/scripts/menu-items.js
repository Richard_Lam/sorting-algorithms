/**
 * Module Description: This module will contain functions to modify the UI's menu tab
 *                     along with the components/functionality to be run by the buttons 
 *                     within the menu tab.
 * 
 * Revision History
 * Date         Ver   Author            Description
 * 6/21/2021    1.0   richard.lam       Initial Version
 */

var MENU_ITEMS_MODULE = (function(){

    let MENU_ITEMS_FUNCTIONS = {};
    
    let menuItemsTab = document.getElementById("menu-items-tab");
    let darkModeSlider = document.getElementById("dark-mode-slider");
    
    let displayClasses = ["body", "top-div", "sort-buttons", "middle-buttons", "setting-dropdowns", "hamburger-menu-icon", "statistics-display",
        "header", "bottom-div", "divider"
    ];
    
    /**
    * @desc Show menu items
    * @param None
    */
    MENU_ITEMS_FUNCTIONS.showMenuItems = function() {
        menuItemsTab.style.width = "185px";
    }
    
    /**
    * @desc Hide menu items
    * @param None
    */
     MENU_ITEMS_FUNCTIONS.closeMenuItems = function() {
        menuItemsTab.style.width = "0%";
    }
    
    /**
    * @desc Toggle on/off dark theme for UI
    * @param None
    */
     MENU_ITEMS_FUNCTIONS.toggleDarkMode = function() {
        if (darkModeSlider.className == "slider") {
            darkModeSlider.className = "slider active";
            toggleDarkTheme();
        }
        else {
            darkModeSlider.className = "slider";
            toggleLightTheme();
        }
    }
    
    function toggleDarkTheme() {
        for (let i=0; i < displayClasses.length; i++) {
            let className = displayClasses[i];
            let classElements = document.getElementsByClassName(className);
            for (let i=0; i < classElements.length; i++) {
                let classElement = classElements[i];
                classElement.className = className + " dark";
            }
        }
        RENDER_MODULE.renderDarkTheme();
    }
    
    function toggleLightTheme() {
        for (let i=0; i < displayClasses.length; i++) {
            let className = displayClasses[i];
            let classElements = document.getElementsByClassName(className);
            for (let i=0; i < classElements.length; i++) {
                let classElement = classElements[i];
                classElement.className = className;
            }
        }
        RENDER_MODULE.renderLightTheme();
    }

    return MENU_ITEMS_FUNCTIONS;
})();