/**
 * Module Description: This file will contain helper functions for
 *                     sorting algorithm functionality.
 * 
 * Revision History
 * Date         Ver   Author            Description
 * 6/21/2021    1.0   richard.lam       Initial Version
 */

/**
* @desc Given an array, swaps indexes i and j
* @param {Number} i
* @param {Number} j
*/
function swap(arr, i, j) {
    [arr[i], arr[j]] = [arr[j], arr[i]];
    SWAP_AND_COMPARISON_MODULE.addSwaps(1);
}

/**
* @desc Returns true/false if array is sorted
* @param {Array} arr
*/
function isArraySorted(arr) {
    for (let i=0; i < arr.length; i++) {
        if (arr[i] > arr[i+1]) {
            return false;
        }        
    }
    return true;
}