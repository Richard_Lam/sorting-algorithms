/**
 * Module Description: This module will contain functions that will render and 
 *                     update elements on the UI.
 * 
 * Revision History
 * Date         Ver   Author            Description
 * 6/21/2021    1.0   richard.lam       Initial Version
 */

var RENDER_MODULE = (function() {

    let RENDER_MODULE_FUNCTIONS = {};

    let DARK_THEME = {
        BACKGROUND_COLOR : "#333333",
        BAR_COLOR : "#7290AA",
        COMPARISON_BAR_COLOR : "#9EC5E5" 
    }
    
    let LIGHT_THEME = {
        BACKGROUND_COLOR : "#FFFFFF",
        BAR_COLOR : "#3DE2CC",
        COMPARISON_BAR_COLOR : "#4F66FF" 
    }
    
    let COLOR_THEME = DARK_THEME;
    let CANVAS_HEIGHT = 450;
    let spaceBetweenBars = 2;
    
    let context = document.getElementById("elements-canvas").getContext("2d");
    
    /**
    * @desc Given an array of indexes which are being compared, draws the value of those indexes in the elements array 
            onto the canvas with the comparison color.
    * @param {Array} arrayOfIndexes - Indexes of elements in array to be rendered
    */
    RENDER_MODULE_FUNCTIONS.renderComparedElements = function(arrayOfIndexes) {
        for (let i=0; i < arrayOfIndexes.length; i++) {
            let index = arrayOfIndexes[i];
            renderElement(index, COLOR_THEME.COMPARISON_BAR_COLOR);
        }
    }
    
    /**
    * @desc Given an array of indexes, draws the value of those indexes in the elements array onto the canvas
            with the bar color.
    * @param {Array} arrayOfIndexes - Indexes of elements in array to be rendered
    */
    RENDER_MODULE_FUNCTIONS.renderElements = function(arrayOfIndexes) {
        for (let i=0; i < arrayOfIndexes.length; i++) {
            let index = arrayOfIndexes[i];
            renderElement(index, COLOR_THEME.BAR_COLOR);
        }
    }
    
    /**
    * @desc Draws all elements in the elements array onto the canvas.
    * @param none;
    */
    RENDER_MODULE_FUNCTIONS.renderAllElements = function() {
        setSpaceBetweenBars();
        for (let i=0; i < elements.length; i++) {
            renderElement(i, COLOR_THEME.BAR_COLOR);
        }
    }

    /**
    * @desc Displays the pause button and hides the sort button
    * @param none;
    */
    RENDER_MODULE_FUNCTIONS.showPauseButton = function() {

        let stopButtons = document.getElementsByClassName("middle-button stop");
        for (let i=0; i < stopButtons.length; i++) {
            let stopButton = stopButtons[i];
            stopButton.style.display = "block";
        }
        let sortButtons = document.getElementsByClassName("middle-button sort");
        for (let i=0; i < sortButtons.length; i++) {
            let sortButton = sortButtons[i];
            sortButton.style.display = "none";
        }
    }
    
    /**
    * @desc Displays the sort button and hides the pause button
    * @param none;
    */
    RENDER_MODULE_FUNCTIONS.showSortButton = function() {
        let stopButtons = document.getElementsByClassName("middle-button stop");
        for (let i=0; i < stopButtons.length; i++) {
            let stopButton = stopButtons[i];
            stopButton.style.display = "none";
        }
        let sortButtons = document.getElementsByClassName("middle-button sort");
        for (let i=0; i < sortButtons.length; i++) {
            let sortButton = sortButtons[i];
            sortButton.style.display = "block";
        }
    }

    /**
    * @desc Adds white border to the sort algorithm passed through the first argument
    * @param {String} selectedSort - Sort algorithm to be highlighted
    */
    RENDER_MODULE_FUNCTIONS.selectSortButton = function(selectedSort) {

        unselectAllSortButtons();

        let quickSortButton = document.getElementById("quicksort-button");
        let bubbleSortButton = document.getElementById("bubblesort-button");
        let heapSortButton = document.getElementById("heapsort-button");
        let circleSortButton = document.getElementById("circlesort-button");

        if (selectedSort == SORTING_ALGORITHM.QUICK_SORT) {
            quickSortButton.style.border = "2px solid white";
            return;
        }
        if (selectedSort == SORTING_ALGORITHM.BUBBLE_SORT) {
            bubbleSortButton.style.border = "2px solid white";
            return;
        }
        if (selectedSort == SORTING_ALGORITHM.HEAP_SORT) {
            heapSortButton.style.border = "2px solid white";
            return;
        }
        if (selectedSort == SORTING_ALGORITHM.CIRCLE_SORT) {
            circleSortButton.style.border = "2px solid white";
            return;
        }
    }

    RENDER_MODULE_FUNCTIONS.renderDarkTheme = function() {
        COLOR_THEME = DARK_THEME;
        RENDER_MODULE_FUNCTIONS.renderAllElements();
    }

    RENDER_MODULE_FUNCTIONS.renderLightTheme = function() {
        COLOR_THEME = LIGHT_THEME;
        RENDER_MODULE_FUNCTIONS.renderAllElements();
    }
    
    /**
    * @desc Draws element in elements array at index
    * @param index - index of element in elements array to be drawn;
    */
    function renderElement(index, color) {
        context.fillStyle = COLOR_THEME.BACKGROUND_COLOR;
        context.fillRect(index*elementWidth, 0, elementWidth, CANVAS_HEIGHT);
        context.fillRect(index*elementWidth, CANVAS_HEIGHT - elements[index], elementWidth, elements[index]);
        context.fillStyle = color;
        context.fillRect(index*elementWidth + spaceBetweenBars, CANVAS_HEIGHT - elements[index], elementWidth - spaceBetweenBars, elements[index]);
    }
    
    /**
    * @desc Adjust space between bars depending on size of elements array
    * @param none
    */
    function setSpaceBetweenBars() {
        if (numOfElements == 1200) {
            spaceBetweenBars = 0;
        }
        else if (numOfElements == 600) {
            spaceBetweenBars = 0;
        }
        else if (numOfElements == 400) {
            spaceBetweenBars = 1;
        }
        else {
            spaceBetweenBars = 2;
        }
    }
    
    /**
    * @desc Remove white border from all sort buttons
    * @param NONE;
    */
    function unselectAllSortButtons() {

        let quickSortButton = document.getElementById("quicksort-button");
        let bubbleSortButton = document.getElementById("bubblesort-button");
        let heapSortButton = document.getElementById("heapsort-button");
        let circleSortButton = document.getElementById("circlesort-button");

        bubbleSortButton.style.border = "none";
        quickSortButton.style.border = "none";
        heapSortButton.style.border = "none";
        circleSortButton.style.border = "none";
    }
    
    return RENDER_MODULE_FUNCTIONS;
})();