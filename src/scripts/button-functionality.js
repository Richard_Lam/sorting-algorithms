var numOfElements = 100;
var elements;
var elementWidth = 1200/numOfElements;

var animationDelay = 1;
var CANVAS_HEIGHT = 450;

var SORTING_ALGORITHM = {
    BUBBLE_SORT : "BubbleSort",
    QUICK_SORT : "QuickSort",
    HEAP_SORT : "HeapSort",
    CIRCLE_SORT : "CircleSort"
}

var selectedSort = SORTING_ALGORITHM.QUICK_SORT;

/**
* @desc Generates new random array to be sorted, stops all sorting algorithms 
*       currently in progress, and shows sort button
* @param None
*/
function handleRandomize() {
    let minHeight = 50;
    elements = [];
    for (var i=0; i < numOfElements; i++) {
        elements.push(Math.floor(minHeight + Math.random() * (CANVAS_HEIGHT - minHeight)));
    }
    stopAllSortingAlgorithms();
    RENDER_MODULE.showSortButton();
    RENDER_MODULE.renderAllElements();
}

/**
* @desc Stops all sorting algorithms and render sort button
* @param None
*/
function handleStop() {
    stopAllSortingAlgorithms();
    RENDER_MODULE.showSortButton();
}

/**
* @desc Stops all sorting algorithms regardless of what stage they are in
* @param None
*/
function stopAllSortingAlgorithms() {
    QUICK_SORT_MODULE.stopQuickSort();
    BUBBLE_SORT_MODULE.stopBubbleSort();
    HEAP_SORT_MODULE.stopHeapSort();
    CIRCLE_SORT_MODULE.stopCircleSort();
}

/**
* @desc Select QuickSort algorithm to be executed once sort button is pressed.
*       Renders white border around QuickSort button.
* @param None
*/
function selectQuickSort() {
    selectedSort = SORTING_ALGORITHM.QUICK_SORT;
    RENDER_MODULE.selectSortButton(selectedSort);
}

/**
* @desc Select BubbleSort algorithm to be executed once sort button is pressed.
*       Renders white border around BubbleSort button.
* @param None
*/
function selectBubbleSort() {
    selectedSort = SORTING_ALGORITHM.BUBBLE_SORT;
    RENDER_MODULE.selectSortButton(selectedSort);
}

/**
* @desc Select HeapSort algorithm to be executed once sort button is pressed.
*       Renders white border around HeapSort button.
* @param None
*/
function selectHeapSort() {
    selectedSort = SORTING_ALGORITHM.HEAP_SORT;
    RENDER_MODULE.selectSortButton(selectedSort);
}

/**
* @desc Select CircleSort algorithm to be executed once sort button is pressed.
*       Renders white border around CircleSort button.
* @param None
*/
function selectCircleSort() {
    selectedSort = SORTING_ALGORITHM.CIRCLE_SORT;
    RENDER_MODULE.selectSortButton(selectedSort);
}

/**
* @desc Executes selected sorting algorithm and show pause button/hides sort button.
* @param None
*/
function handleSort() {
    if (selectedSort == SORTING_ALGORITHM.QUICK_SORT) {
        QUICK_SORT_MODULE.startQuickSort();
    }
    else if (selectedSort == SORTING_ALGORITHM.BUBBLE_SORT) {
        BUBBLE_SORT_MODULE.startBubbleSort();
    }
    else if (selectedSort == SORTING_ALGORITHM.HEAP_SORT) {
        HEAP_SORT_MODULE.startHeapSort();
    }
    else if (selectedSort == SORTING_ALGORITHM.CIRCLE_SORT) {
        CIRCLE_SORT_MODULE.startCircleSort();
    }
    SWAP_AND_COMPARISON_MODULE.resetSwapsAndComparisons();
    RENDER_MODULE.showPauseButton();
}

function selectSortFromDropdown() {
    selectedSort = this.value;
}

/**
* @desc Updates number of elements in elements array to value in elements dropdown
* @param None
*/
function updateNumberOfElements() {
    numOfElements = this.value;
    elementWidth = 1200/numOfElements;
    handleRandomize();
}

/**
* @desc Updates animation delay to value in elements dropdown
* @param None
*/
function updateAnimationDelay() {
    animationDelay = this.value;
}

/**
* @desc Handles changing the layout when the window is resized
* @param None
*/
function resizeWindow() {

    function hideAllLayouts() {
        desktopLayout.style.display = "none";
        smallDesktopLayout.style.display = "none";
        mediumDesktopLayout.style.display = "none";
        handheldDeviceLayout.style.display = "none";
    }

    function setHeightOfMainContainer() {
        if (window.innerWidth <= 375) {
            mainContainer.style.height = "400px";
        }
        else if (window.innerWidth <= 700) {
            if (window.innerHeight < 500) {
                mainContainer.style.height = "225px";
            }
            else if (window.innerHeight < 600) {
                mainContainer.style.height = "325px";
            }
            else {
                mainContainer.style.height = "450px";
            }
        }
        else if (window.innerWidth <= 950) {
            if (window.innerHeight < 500) {
                mainContainer.style.height = "250px";
            }
            else if (window.innerHeight < 600) {
                mainContainer.style.height = "375px";
            }
            else {
                mainContainer.style.height = "475px";
            }
        }
        else {
            if (window.innerHeight < 510) {
                mainContainer.style.height = "350px";
            }
            else if (window.innerHeight < 600) {
                mainContainer.style.height = "450px";
            }
            else {
                mainContainer.style.height = "500px";
            }
        }
    }
    function selectCurrentSortAlgorithm() {
        
    }

    let handheldDeviceLayout = document.getElementsByClassName("handheld-layout")[0];
    let smallDesktopLayout = document.getElementsByClassName("small-desktop-layout")[0];
    let mediumDesktopLayout = document.getElementsByClassName("medium-desktop-layout")[0];
    let desktopLayout = document.getElementsByClassName("desktop-layout")[0];

    let mainContainer = document.getElementsByClassName("main-container")[0];
    let bottomDiv = document.getElementsByClassName("bottom-div")[0];

    if (window.innerWidth <= 375) {

        hideAllLayouts();
        handheldDeviceLayout.style.display = "block";
        bottomDiv.style.display = "flex";

        setHeightOfMainContainer();
    }
    else if (window.innerWidth <= 700) {

        hideAllLayouts();
        smallDesktopLayout.style.display = "block";
        bottomDiv.style.display = "none";

        setHeightOfMainContainer();
    }
    else if (window.innerWidth <= 950) {

        hideAllLayouts();
        mediumDesktopLayout.style.display = "block";
        bottomDiv.style.display = "none";

        setHeightOfMainContainer();
    }
    else {

        hideAllLayouts();
        desktopLayout.style.display = "block";
        bottomDiv.style.display = "none";

        setHeightOfMainContainer();
    }
}

/**
* @desc Executes all functions to be run on page loading.
* @param None
*/
function onLoad() {
    
    let elementSelectors = document.getElementsByClassName("drop-down num-of-elements-selector");
    for (let i=0; i < elementSelectors.length; i++) {
        elementSelectors[i].onchange = updateNumberOfElements;
    }
    let delaySelectors = document.getElementsByClassName("drop-down delay-selector");
    for (let i=0; i < elementSelectors.length; i++) {
        delaySelectors[i].onchange = updateAnimationDelay;
    }
    let sortAlgorithmSelectors = document.getElementsByClassName("drop-down sort-algorithm-selector");
    for (let i=0; i < sortAlgorithmSelectors.length; i++) {
        sortAlgorithmSelectors[i].onchange = selectSortFromDropdown;
    }

    resizeWindow();
    handleRandomize();
    RENDER_MODULE.renderAllElements();
}

onLoad();