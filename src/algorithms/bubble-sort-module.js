/**
 * Module Description: This module will contain two functions to start and
 *                     stop the bubble sort algorithm.
 * 
 * Revision History
 * Date         Ver   Author            Description
 * 6/21/2021    1.0   richard.lam       Initial Version
 */

var BUBBLE_SORT_MODULE = (function() {
    let BUBBLE_SORT_FUNCTIONS = {};

    let stopBubbleAlgorithm = false;

    /**
    * @desc Runs BubbleSort algorithm onto elements array
    * @param None
    */
    BUBBLE_SORT_FUNCTIONS.startBubbleSort = function () {
        stopBubbleAlgorithm = false;
        checkArrayIfSorted(false);
    }

    /**
    * @desc Stops BubbleSort algorithm regardless of what stage of sorting it is in
    * @param None
    */
    BUBBLE_SORT_FUNCTIONS.stopBubbleSort = function() {
        stopBubbleAlgorithm = true;
    }

    function checkArrayIfSorted(sorted) {
    
        if (stopBubbleAlgorithm) {
            return;
        }
        
        if (!sorted) {
            sorted = true;
            iterateArray(0, sorted);
        }
        else {
            RENDER_MODULE.showSortButton();
        }
    }
    
    function iterateArray(i, sorted) {
        
        if (stopBubbleAlgorithm) {
            return;
        }
    
        if (i < numOfElements-1) {
            RENDER_MODULE.renderComparedElements([i, i+1]);
            setTimeout(()=>compareAdjacentElements(i, sorted), animationDelay);
        }
        else {
            checkArrayIfSorted(sorted);
        }
    }
    
    function compareAdjacentElements(i, sorted) {
        SWAP_AND_COMPARISON_MODULE.addComparisons(1);
        if (elements[i] > elements[i+1]) {
            sorted = false;
            swap(elements, i, i+1);
        }
        RENDER_MODULE.renderElements([i,i+1]);
        i++;
        setTimeout(()=>iterateArray(i, sorted), animationDelay);
    }

    return BUBBLE_SORT_FUNCTIONS;
})();