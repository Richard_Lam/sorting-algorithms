/**
 * Module Description: This module will contain two functions to start and
 *                     stop the HeapSort algorithm.
 * 
 * Revision History
 * Date         Ver   Author            Description
 * 6/21/2021    1.0   richard.lam       Initial Version
 */

var HEAP_SORT_MODULE = (function() {

    let HEAP_SORT_FUNCTIONS = {};

    let stopHeapAlgorithm = false;

    /**
    * @desc Runs HeapSort algorithm onto elements array
    * @param None
    */
    HEAP_SORT_FUNCTIONS.startHeapSort = function() {
        stopHeapAlgorithm = false;
        heapSort(elements);
    }
    
    /**
    * @desc Stops Heap Sort algorithm regardless of what stage of sorting it is in
    * @param None
    */
    HEAP_SORT_FUNCTIONS.stopHeapSort = function() {
        stopHeapAlgorithm = true;
    }

    /**
    * @desc Sorts array of elements using HeapSort algorithm
    * @param {Array} arr - array to be sorted
    */
    function heapSort(arr) {
        let mid = Math.floor(arr.length/2) - 1;
        heapifyArr(arr, mid);
    }

    function heapifyArr(arr, mid) {

        if (stopHeapAlgorithm) {
            return;
        }

        let length = arr.length;
        if (mid >= 0) {
            initialHeapify(arr, length, mid);
        }
        else {
            // Runs once initial heapify is done
            let headOfSortedArr = arr.length - 1;
            moveLargestElementInHeapToSortedArr(arr, headOfSortedArr);
        }
    }

    function initialHeapify(arr, length, indexOfParentNode) {

        function continueInitialHeapify() {
            swap(arr, indexOfParentNode, largest);
            RENDER_MODULE.renderElements([indexOfParentNode, largest]);
            setTimeout(()=>initialHeapify(arr, length, largest), animationDelay);
        }

        if (stopHeapAlgorithm) {
            return;
        }

        let largest = indexOfParentNode;
        let left = indexOfParentNode*2 + 1;
        let right = left + 1;

        SWAP_AND_COMPARISON_MODULE.addComparisons(1);
        if (left < length && arr[left] > arr[largest]) {
            largest = left;
        }

        SWAP_AND_COMPARISON_MODULE.addComparisons(1);
        if (right < length && arr[right] > arr[largest]) {
            largest = right;
        }

        SWAP_AND_COMPARISON_MODULE.addComparisons(1);
        if (largest != indexOfParentNode) {
            RENDER_MODULE.renderComparedElements([largest, indexOfParentNode]);
            setTimeout(()=>continueInitialHeapify(), animationDelay);
        }
        else {
            indexOfParentNode--;
            heapifyArr(arr, indexOfParentNode);
        }
    }

    function moveLargestElementInHeapToSortedArr(arr, headOfSortedArr) {
        
        if (stopHeapAlgorithm) {
            return;
        }
        
        let indexOfLargestNode = 0;
        if (headOfSortedArr >= 0) {
            swap(arr, indexOfLargestNode, headOfSortedArr);
            RENDER_MODULE.renderElements([indexOfLargestNode, headOfSortedArr]);
            setTimeout(()=>reHeapify(arr, headOfSortedArr, indexOfLargestNode), animationDelay);
        }
        else {
            // Runs once heap sort completes
            RENDER_MODULE.showSortButton();
        }
    }

    function reHeapify(arr, length, indexOfParentNode) {

        function continueReHeapify() {
            swap(arr, indexOfParentNode, largest);
            RENDER_MODULE.renderElements([indexOfParentNode, largest]);
            setTimeout(()=>reHeapify(arr, length, largest), animationDelay);
        }

        if (stopHeapAlgorithm) {
            return;
        }

        let largest = indexOfParentNode;
        let left = indexOfParentNode*2 + 1;
        let right = left + 1;

        SWAP_AND_COMPARISON_MODULE.addComparisons(1);
        if (left < length && arr[left] > arr[largest]) {
            largest = left;
        }

        SWAP_AND_COMPARISON_MODULE.addComparisons(1);
        if (right < length && arr[right] > arr[largest]) {
            largest = right;
        }

        SWAP_AND_COMPARISON_MODULE.addComparisons(1);
        if (largest != indexOfParentNode) {
            RENDER_MODULE.renderComparedElements([indexOfParentNode, largest]);
            setTimeout(()=>continueReHeapify(), animationDelay);
        }
        else {
            length--;
            moveLargestElementInHeapToSortedArr(arr, length);
        }
    }

    return HEAP_SORT_FUNCTIONS;

})();