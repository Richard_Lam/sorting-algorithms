/**
 * Module Description: This module will contain two functions to start and
 *                     stop the QuickSort algorithm.
 * 
 * Revision History
 * Date         Ver   Author            Description
 * 6/21/2021    1.0   richard.lam       Initial Version
 */

var QUICK_SORT_MODULE = (function() {
    let QUICK_SORT_FUNCTIONS = {};

    let stopQuickSortAlgorithm = false;

    /**
    * @desc Runs QuickSort algorithm onto elements array
    * @param None
    */
    QUICK_SORT_FUNCTIONS.startQuickSort = function() {
        stopQuickSortAlgorithm = false;
        quickSort(0, numOfElements-1, 0, -1);
    }

    /**
    * @desc Stops QuickSort algorithm regardless of what stage of sorting it is in
    * @param None
    */
    QUICK_SORT_FUNCTIONS.stopQuickSort = function() {
        stopQuickSortAlgorithm = true;
    }

    function quickSort(start, end, i, j) {

        if (stopQuickSortAlgorithm) {
            return;
        }
    
        if (start < end) {
            if (j == -1) {
                i = start - 1;
                j = start;
            }
            if (j < end) {
                RENDER_MODULE.renderComparedElements([j, end]);
                setTimeout(()=>compareJthElementWithPivot(start, end, i, j), animationDelay);
            }
            else {
                movePivotAfterIthElement(i, end);
                RENDER_MODULE.renderAllElements();
                
                sortElementsBeforePivot(start, i);
                sortElementsAfterPivot(i, end);
            }
        }
        if (isArraySorted(elements)) {
            RENDER_MODULE.showSortButton();
        }
    }
    
    function compareJthElementWithPivot(start, end, i, j) {
        SWAP_AND_COMPARISON_MODULE.addComparisons(1);
        if (elements[j] < elements[end]) {
            i++;
            swap(elements, i, j);
        }
        RENDER_MODULE.renderElements([i, j]);
        j++;
        setTimeout(()=>quickSort(start, end, i, j), animationDelay);
    }
    
    function movePivotAfterIthElement(i, end) {
        SWAP_AND_COMPARISON_MODULE.addSwaps(1);
        elements.splice(i+1, 0, elements[end]);
        elements.splice(end+1, 1);
    }
    
    function sortElementsBeforePivot(start, i) {
        setTimeout(()=>quickSort(start, i, i, -1), animationDelay);
    }
    
    function sortElementsAfterPivot(i, end) {
        setTimeout(()=>quickSort(i+2, end, i, -1), animationDelay);
    }

    return QUICK_SORT_FUNCTIONS;
})();