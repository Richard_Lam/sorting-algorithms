/**
 * Module Description: This module will contain two functions to start and
 *                     stop the CircleSort algorithm.
 * 
 * Revision History
 * Date         Ver   Author            Description
 * 6/21/2021    1.0   richard.lam       Initial Version
 */

var CIRCLE_SORT_MODULE = (function(){
    
    let CIRCLE_SORT_FUNCTIONS = {};

    let stopCircleAlgorithm = false;
    let swaps;
    let numOfSingleElements;

    CIRCLE_SORT_FUNCTIONS.startCircleSort = function() {
        stopCircleAlgorithm = false;
        swaps = 1;
        startOneCircleSortInstance();
    }

    CIRCLE_SORT_FUNCTIONS.stopCircleSort = function() {
        stopCircleAlgorithm = true;
    }
    
    function startOneCircleSortInstance() {
        if (stopCircleAlgorithm) return;
        if (swaps) {
            swaps = 0;
            numOfSingleElements = 0;
            circleSort(elements, 0, numOfElements-1, 0);
        }
        else {
            // TO DO : Refactor functions to execute once sort finishes to a separate function
            RENDER_MODULE.showSortButton();
        }
    }

    function circleSort(arr, start, end) {

        if (stopCircleAlgorithm) return;
    
        if (start == end) {
            numOfSingleElements++;
            if (numOfSingleElements == numOfElements) {
                startOneCircleSortInstance();
            }
            return;
        }
        let left = start;
        let right = end;
        if (left < right) {
            compareElementsAtOppositeEnds(arr, start, end, left, right);
        }
    }
    
    function compareElementsAtOppositeEnds(arr, start, end, left, right) {
    
        function compareLeftElementWithRightElement(arr, start, end, left, right) {
            SWAP_AND_COMPARISON_MODULE.addComparisons(1);
            if (arr[left] > arr[right]) {
                swap(arr, left, right);
                swaps++;
            }
            RENDER_MODULE.renderElements([left, right]);
            left++;
            right--;
            setTimeout(()=>compareElementsAtOppositeEnds(arr, start, end, left, right), animationDelay);
        }
    
        if (stopCircleAlgorithm) return;
    
        if (left < right) {
            RENDER_MODULE.renderComparedElements([left, right]);
            setTimeout(()=>compareLeftElementWithRightElement(arr, start, end, left, right), animationDelay);
        }
        else {
            compareMiddleElementWithElementToRight(arr, start, end, left, right);
        }
    }
    
    function compareMiddleElementWithElementToRight(arr, start, end, left, right) {
    
        function continueComparison(arr, start, end, left, right) {
            SWAP_AND_COMPARISON_MODULE.addComparisons(1);
            if (arr[left] > arr[right+1]) {
                swap(arr, left, right+1);
                swaps++;
            }
            RENDER_MODULE.renderElements([left, right+1]);
            left++;
            right--;
            setTimeout(()=>compareMiddleElementWithElementToRight(arr, start, end, left, right), animationDelay);
        }
        
        if (stopCircleAlgorithm) return;
        
        if (left == right) {
            RENDER_MODULE.renderComparedElements([left, right+1]);
            setTimeout(()=>continueComparison(arr, start, end, left, right), animationDelay);
        }
        else {
            partitionArrayIntoTwoHalves(arr, start, end);
        }
    }
    
    function partitionArrayIntoTwoHalves(arr, start, end) {
    
        if (stopCircleAlgorithm) return;
    
        let mid = Math.floor((end - start)/2);
        circleSort(arr, start, start + mid);
        circleSort(arr, start + mid + 1, end);
    }

    return CIRCLE_SORT_FUNCTIONS;    
})();