SortVisual is a sorting algorithm visualizer for the purpose of visualizing how a sorting algorithm such as QuickSort or CircleSort sorts an array of elements. 
This application has:
    -four sorting algorithms: BubbleSort, QuickSort, HeapSort, and CircleSort
    -a responsive user interface (the layout will adapt to whether it is being opened on PC or a handheld device).
    -a light/dark theme which is toggled via the slider inside the hamburger menu icon
    -adjustable number of elements to sort
    -adjustable speeds at which to execute the sorting algorithm
You can access it here: https://richard_lam.gitlab.io/sorting-algorithms/